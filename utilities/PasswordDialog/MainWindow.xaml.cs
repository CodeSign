﻿/******************************************************************************/
/* CodeSign, by LoRd_MuldeR <MuldeR2@GMX.de>                                  */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Threading;

namespace PasswordDialog
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        private readonly DispatcherTimer dispatcherTimer;
        private readonly WindowInteropHelper interopHelper;

    public MainWindow()
        {
            InitializeComponent();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Background);
            interopHelper = new WindowInteropHelper(this);
        }

        //-------------------------------------------------------------------
        // Event Handlers
        //-------------------------------------------------------------------

        protected override void OnContentRendered(EventArgs e)
        {
            IntPtr handle = interopHelper.Handle;
            SetForegroundWindow(handle);
            SetWindowPos(handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);

            PasswordBox.Focus();

            dispatcherTimer.Tick += OnTimerTick;
            dispatcherTimer.Interval = TimeSpan.FromMinutes(3);
            dispatcherTimer.Start();
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            if (Button_OK.IsEnabled)
            {
                using (SecureWrapper wrapper = new SecureWrapper(PasswordBox.SecurePassword))
                {
                    byte[] buffer = wrapper.ByteBuffer;
                    using (Stream stdout = Console.OpenStandardOutput())
                    {
                        stdout.Write(buffer, 0, buffer.Length);
                        stdout.Flush();
                    }
                }
                Close(); /*password was entered, now exit!*/
            }
        }

        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            Close(); /*aborted*/
        }

        private void Window_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                e.Handled = true;
                Close();
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Button_OK.IsEnabled = (PasswordBox.SecurePassword.Length > 0);
        }

        private void PasswordBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                e.Handled = true;
                Dispatcher.InvokeAsync(() => Button_OK_Click(sender, e));
            }
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            dispatcherTimer.Stop();
            Close();
        }

        //-------------------------------------------------------------------
        // Native Methods
        //-------------------------------------------------------------------

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
    }
}
