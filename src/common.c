/******************************************************************************/
/* CodeSign, by LoRd_MuldeR <MuldeR2@GMX.de>                                  */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#include "common.h"
#include <string.h>
#include <ctype.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN 1
#include <Windows.h>
#include <io.h>
#include <fcntl.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif

#include <openssl/crypto.h>

/*-------------------------------------------------------*/
/* print_logo() / print_license()                        */
/*-------------------------------------------------------*/

void print_logo(const char *const app_name)
{
#ifdef _WIN32
    SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX);
    _setmode(_fileno(stdin), _O_BINARY);
#endif
    fprintf(stderr, "CodeSign - %s [" __DATE__ "], by LoRd_MuldeR <mulder2@gmx.de>\n", app_name);
    fprintf(stderr, "using %.15s\n\n", OpenSSL_version(OPENSSL_VERSION));
    fflush(stderr);
}

void print_license(void)
{
    fputs("This work has been released under the CC0 1.0 Universal license!\n", stderr);
    fputs("Please see http://www.muldersoft.com for additional information.\n\n", stderr);
}

/*-------------------------------------------------------*/
/* int2uint()                                            */
/*-------------------------------------------------------*/

unsigned int int2uint(const int value)
{
    return (value < 0) ? 0U : ((unsigned int)value);
}

/*-------------------------------------------------------*/
/* convert_wchar_to_UTF8()                               */
/*-------------------------------------------------------*/

char *convert_CHAR_to_UTF8(const CHAR_T *str)
{
    char *str_utf8 = NULL;
#ifdef _WIN32
    if (str)
    {
        int bytes = WideCharToMultiByte(CP_UTF8, 0, str, -1, NULL, 0, NULL, NULL);
        if (bytes > 0)
        {
            str_utf8 = OPENSSL_zalloc(bytes);
            if(str_utf8)
            {
                if (WideCharToMultiByte(CP_UTF8, 0, str, -1, str_utf8, bytes, NULL, NULL) == 0) 
                {
                    OPENSSL_clear_free(str_utf8, bytes);
                    return NULL;
                }
            }
        }
    }
#else
    str_utf8 = OPENSSL_strdup(str);
#endif
    return str_utf8;
}

/*-------------------------------------------------------*/
/* read_line_from_file()                                 */
/*-------------------------------------------------------*/

char *read_line_from_file(FILE *const file, const int trim)
{
    char buffer[4096], *line;
    size_t len;
    while(!(feof(file) || ferror(file)))
    {
        if((line = fgets(buffer, sizeof(buffer), file)))
        {
            if (trim)
            {
                while (*line && isspace(*line))
                {
                    ++line;
                }
            }
            len = strlen(line);
            while (len > 0)
            {
                if ((line[len - 1U] != '\r') && (line[len - 1U] != '\n'))
                {
                    break;
                }
                line[--len] = '\0';
            }
            if (trim)
            {
                while (len > 0)
                {
                    if (!isspace(line[len - 1U]))
                    {
                        break;
                    }
                    line[--len] = '\0';
                }
            }
            if (len > 0)
            {
                return OPENSSL_strdup(line);
            }
        }
    }
    return NULL;
}

/*-------------------------------------------------------*/
/* get_current_time()                                    */
/*-------------------------------------------------------*/

#define FILETIME_1970 116444736000000000ULL

#ifdef _WIN32

static inline uint64_t filetime_to_ui64(const FILETIME *const file_time)
{
    ULARGE_INTEGER result;
    result.HighPart = file_time->dwHighDateTime;
    result. LowPart = file_time-> dwLowDateTime;
    return result.QuadPart;
}

static uint64_t get_system_time(void)
{
    FILETIME system_time;
    GetSystemTimeAsFileTime(&system_time);
    return filetime_to_ui64(&system_time);
}

uint64_t get_current_time_usec(void)
{
    const uint64_t system_time = get_system_time();
    if (system_time >= FILETIME_1970)
    {
        return (system_time - FILETIME_1970) / 10ULL;
    }
    return 0ULL;
}

#else

uint64_t get_current_time_usec(void)
{
    struct timeval t;
    if(gettimeofday(&t, NULL) == 0)
    {
        return (((uint64_t)t.tv_sec) * 1000000ULL) + ((uint64_t)t.tv_usec);
    }
    return 0ULL;
}

#endif /*_WIN32*/

/*-------------------------------------------------------*/
/* store_uint64() / load_uint64()                        */
/*-------------------------------------------------------*/

void store_uint64(unsigned char *const buffer, const uint64_t value)
{
    buffer[0U] = (uint8_t)(value >> 56);
    buffer[1U] = (uint8_t)(value >> 48);
    buffer[2U] = (uint8_t)(value >> 40);
    buffer[3U] = (uint8_t)(value >> 32);
    buffer[4U] = (uint8_t)(value >> 24);
    buffer[5U] = (uint8_t)(value >> 16);
    buffer[6U] = (uint8_t)(value >>  8);
    buffer[7U] = (uint8_t)(value);
}

uint64_t load_uint64(const unsigned char *const buffer)
{
    return
        (((uint64_t)buffer[0U]) << 56) |
        (((uint64_t)buffer[1U]) << 48) |
        (((uint64_t)buffer[2U]) << 40) |
        (((uint64_t)buffer[3U]) << 32) |
        (((uint64_t)buffer[4U]) << 24) |
        (((uint64_t)buffer[5U]) << 16) |
        (((uint64_t)buffer[6U]) <<  8) |
        (((uint64_t)buffer[7U]));
}

/*-------------------------------------------------------*/
/* force_flush()                                         */
/*-------------------------------------------------------*/

int force_flush(FILE *const file)
{
    const int result = fflush(file);
#ifdef _WIN32
    FlushFileBuffers((HANDLE)_get_osfhandle(_fileno(file)));
#else
    fsync(fileno(file));
#endif
    return (result != EOF);
}

/*-------------------------------------------------------*/
/* load_resource_data()                                  */
/*-------------------------------------------------------*/

#ifdef _WIN32

const unsigned char *load_resource_data(const wchar_t *const name, unsigned int *const length)
{
    HRSRC resource = FindResourceW(NULL, name, RT_RCDATA);
    if (resource)
    {
        *length = SizeofResource(NULL, resource);
        if (*length > 0U)
        {
            HGLOBAL data = LoadResource(NULL, resource);
            if (data)
            {
                return (const unsigned char*) LockResource(data);
            }
        }
    }
    *length = 0U;
    return NULL;
}

#endif /*_WIN32*/
